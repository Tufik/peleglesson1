#include "stack.h"


/*
	Function finds the last element of the stack.

	Input:
			s - The stack to search in.

	Output:
			The address to the last element in the stack.
*/
node* oneBeforeLastElement(stack* s)
{
	node* parser = s->_elements;

	if (!(parser->_next))
	{
		return parser;
	}

	while (parser->_next->_next)
	{
		parser = parser->_next;
	}

	return parser;
}


/*
	Function adds an element to the top of the stack.

	Input:
			s - The stack to push into.
			element - The value to assign to the element.

	Output:
			void.
*/
void push(stack* s, unsigned int element)
{
	node* newElement = new node;

	newElement->_data = element;
	newElement->_next = nullptr;

	newElement->_next = s->_elements;

	s->_elements = newElement;

	s->_size++;
}

/*
	Function removes the last element of the list.

	Input:
			s - The stack to remove the first item from.

	Output:
			The removed element or -1 if the list is empty.
*/
int pop(stack* s)
{
	int value = -1;
	node* lastNode = nullptr;

	if (s->_size > 0)
	{
		lastNode = s->_elements->_next;
		value = s->_elements->_data;
		delete s->_elements;
		s->_elements = lastNode;
	}

	return value;
}



/*
	Function initiallizes the stack.

	Input:
			s - The stack to initiallize.
	Output:
			void.
*/
void initStack(stack* s)
{
	s->_elements = nullptr;
	s->_size = 0;
}



/*
	Function clears the stack.

	Input:
			s - The stack to deallocate.

	Output:
			void.
*/
void cleanStack(stack* s)
{
	while (pop(s) != -1)
	{}
}