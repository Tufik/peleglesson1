#include "utils.h"

#define SIZE 5

int main()
{
	int i = 0;
	int* arr = reverse10();

	for (i = 0; i < 10; i++)
	{
		std::cout << arr[i] << std::endl;
	}
	
	delete[] arr;

	std::cin.get();
	return 0;
}