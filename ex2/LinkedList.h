#pragma once

#include <iostream>

typedef struct node
{
	unsigned int _data;
	struct node* _next;
} node;

void addNode(node*& list, unsigned int value);
int removeNode(node*& list);