#include "LinkedList.h"


/*
	Function adds a new element to the list.

	Input:
			s - The node to add.
			value - The value of the new element.

	Output:
			void.
*/
void addNode(node*& list, unsigned int value)
{
	node* newElement = new node;
	node* temp = list;

	newElement->_data = value;
	newElement->_next = nullptr;
	
	if (!list)
	{
		list = newElement;
	}

	else
	{
		newElement->_next = list;
		list = newElement;
	}
}


/*
	Function removes the first element of the list.

	Input:
			s - The list to remove the first item from.

	Output:
			The removed element or -1 if the list is empty.
*/
int removeNode(node*& list)
{
	node* dyingElement = list;
	int value = -1;
	
	if (list)
	{
		value = dyingElement->_data;
		list = list->_next;
		delete dyingElement;
	}

	return value;
}


/*
	Function prints out the contents of the list.

	Input:
			s - The list to print.

	Output:
			void.
*/
void printList(node* list)
{
	while (list)
	{
		std::cout << list->_data << std::endl;
		list = list->_next;
	}
}