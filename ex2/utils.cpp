#include "utils.h"

void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* myStack = new stack;

	initStack(myStack);

	for (i = 0; i < size; i++)
	{
		push(myStack, nums[i]);
	}

	for (i = 0; i < size; i++)
	{
		nums[i] = pop(myStack);
	}
}

/*
	Function returns the reversed array that the user inputted.

	Input:
			void.

	Output:
			The reversed array.
*/
int* reverse10()
{
	int i = 0;
	int* arr = new int[10];

	std::cout << "Enter 10 numbers:" << std::endl;
	for (i = 0; i < 10; i++)
	{
		std::cin >> arr[i];
	}

	reverse(arr, 10);

	return arr;
}