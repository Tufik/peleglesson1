#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* elements;
	unsigned int size;
} queue;

void initQueue(queue*& q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

bool isEmpty(queue* q);
void printQueue(queue* q);

#endif /* QUEUE_H */