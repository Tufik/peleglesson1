#include "queue.h"
#include <iostream>

int main()
{
	return 0;
}

/*
	Function checks if the queue is empty.

	Input:
			q - The queue to check.

	Output:
			"Is the queue empty?"
*/
bool isEmpty(queue* q)
{
	return q->size == 0;
}

/*
	Function initializes the queue.

	Input:
			q - The queue to initialize.
			size - The number of elements to in the queue.

	Output:
			void.
*/
void initQueue(queue*& q, unsigned int size)
{
	q = new queue;

	q->elements = new unsigned int[size];
	q->size = size;
}

/*
	Function deallocates the queue.

	Input:
			q - The queue to deallocate.

	Output:
			void.
*/
void cleanQueue(queue* q)
{
	delete[] q->elements;
	delete q;
}

/*
	Function adds a new element to the queue.

	Input:
			q - The queue to append to.
			newValue - The value of the new element.

	Output:
			void.
*/
void enqueue(queue* q, unsigned int newValue)
{
	unsigned int* tempArr = q->elements;
	
	q->size++;
	q->elements = new unsigned[q->size];

	memcpy(q->elements, tempArr, q->size - 1);
	q->elements[q->size - 1] = newValue;

	delete[] tempArr;
}

/*
	Function pops the first element from the queue.

	Input:
			q - The queue to pop from.

	Output:
			The poped element or -1 if the queue is empty.
*/
int dequeue(queue* q)
{
	unsigned int* tempArr = q->elements;
	unsigned int poped = q->elements[q->size - 1];

	if (isEmpty(q))
	{
		return -1;
	}

	else
	{
		q->size--;
		q->elements = new unsigned int[q->size];
		memcpy(q->elements, tempArr + 1, q->size);

		delete[] tempArr;

		return poped;
	}
}

/*
	Function prints out the queue.

	Input:
			q - The queue to print.

	Output:
			void.
*/
void printQueue(queue* q)
{
	int i = 0;

	if (!isEmpty(q))
	{
		for (i = 0; i < q->size; i++)
		{
			std::cout << q->elements[i] << " ";
		}
	}
	std::cout << std::endl;
}